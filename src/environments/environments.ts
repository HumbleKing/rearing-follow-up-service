export const env = {
  project: {
    name: 'Rearing API',
    description: 'Rearing API',
    poweredBy: 'LDB',
    email: 'lamine.thiam@mdb.sn',
    port: 3000,
  },
  microservices: {
    headers: { 'content-type': 'application/json' },
  },
  api: {
    morgan: 'dev',
    responseTime: {
      header: false,
      flag: 'response-timer',
    },
    pagination: {
      limit: 10,
      page: 1,
      skip: 0,
    },
  },
  upload: {
    path: '../data/uploads',
    size: '10mb',
  },
  cors: {
    origin: true,
    methods: ['GET', 'PATCH', 'POST', 'DELETE', 'PUT'],
    allowedHeaders: ['Content-Type', 'Authorization', 'Content-Length'],
    exposedHeaders: ['x-provider', 'limit', 'page', 'count', 'X-Response-Time'],
  },
  mongodb: {
    uri: 'mongodb://database:27017/rearing',
    uriTest: 'mongodb://database:27017/rearing_dev',
    debug: false,
  },
};
