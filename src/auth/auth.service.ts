import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { NotFoundException } from 'src/exceptions/notfound-exception';
import { Result, succeed } from 'src/helpers/http-response';
import { getDefaultUserInfos } from 'src/helpers/users.helper';
import { UserDocument } from 'src/users/users.entity';

import { UsersService } from '../users/users.service';
import { AuthDto } from './auth.dto';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async login(authDto: AuthDto): Promise<Result> {
    const user = await this.validateUser(authDto);
    const payload = {
      userId: user._id,
    };
    const data = {
      user: getDefaultUserInfos(user),
      access_token: this.jwtService.sign(payload),
    };
    return succeed({ data });
  }

  async validateUser(authDto: AuthDto): Promise<UserDocument> {
    const { code, password } = authDto;
    const user = await this.usersService.findUserByCode(code);
    if (!user) {
      throw new NotFoundException('User not found!');
    }
    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
      throw new UnauthorizedException(
        'Wrong connection information. Try again',
      );
    }
    return user;
  }
}
