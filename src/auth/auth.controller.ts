import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { AuthDto } from './auth.dto';

import { AuthService } from './auth.service';
import { JwtAuthGuard } from './jwt-auth.gard';

@Controller('authentification')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('/login')
  async login(@Body() authDto: AuthDto) {
    return this.authService.login(authDto);
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  async test() {
    return 'Success!';
  }
}
