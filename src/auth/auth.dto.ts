import { IsEmail, IsNotEmpty } from 'class-validator';

export class AuthDto {
  @IsNotEmpty({ message: "User's code is required. " })
  code: string;

  @IsNotEmpty({ message: "User's password is required. " })
  password: string;
}
