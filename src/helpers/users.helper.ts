import { User } from 'src/users/users.entity';

export const isValidUserCode = (code: string) =>
  code && code.includes('USR-') && code.length === 23;

export const getDefaultUserInfos = (user: User) => ({
  code: user?.code,
  firstName: user?.firstName,
  lastName: user?.lastName,
  isDeleted: user?.isDeleted,
  email: user?.email,
  createdAt: user?.createdAt,
  lastUpdatedDate: user?.lastUpdatedAt,
});
