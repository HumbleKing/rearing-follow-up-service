import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsOptional, Length } from 'class-validator';

export class NewUserDto {
  @IsNotEmpty({ message: 'User first name is required.' })
  firstName: string;

  @IsNotEmpty({ message: 'User last name is required.' })
  lastName: string;

  @IsEmail({}, { message: 'Invalid user email.' })
  email: string;

  @IsNotEmpty({ message: "Users's CNI is required." })
  @Length(13, 13, { message: "User's CNI must be 13 characters" })
  cni: string;

  @ApiProperty({ required: false })
  @IsOptional()
  birthdate: string;
}

export class UpdateUserDto {
  @ApiProperty({ required: false })
  @IsOptional()
  @IsNotEmpty({
    message:
      "First name's value can't be empty. You can remove it from object if it's not updated.",
  })
  firstName: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsNotEmpty({
    message:
      "Last name's value can't be empty. You can remove it from object if it's not updated.",
  })
  lastName: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsNotEmpty({
    message:
      "Email's value can't be empty. You can remove it from object if it's not updated",
  })
  @IsEmail({}, { message: 'Invalid user email.' })
  email: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsNotEmpty({
    message:
      "Users's CNI can't be empty. You can remove it from object if it's not updated",
  })
  @Length(13, 13, { message: "User's CNI must be 13 characters" })
  cni: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsNotEmpty({
    message:
      "Users's birthdate can't be empty. You can remove it from object if it's not updated",
  })
  birthdate: string;
}
