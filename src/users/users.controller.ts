// eslint-disable-next-line prettier/prettier
import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
// eslint-disable-next-line prettier/prettier
import { ApiBadRequestResponse, ApiCreatedResponse, ApiInternalServerErrorResponse, ApiNotFoundResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { InvalidCodeException } from 'src/exceptions/invalid-code-exception';
import { isValidUserCode } from 'src/helpers/users.helper';

import { NewUserDto, UpdateUserDto } from './users.dto';
import { User } from './users.entity';
import { UsersService } from './users.service';

@ApiTags('Users')
@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) {}

  @ApiOkResponse({
    description: '',
    type: User,
    isArray: true,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error occured.',
  })
  @Get()
  async findAll() {
    return await this.usersService.findAll();
  }

  @ApiCreatedResponse({
    description: 'New user successfully created.',
    type: User,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error occured.',
  })
  @ApiBadRequestResponse({
    description:
      'Bad Request. most often duplicated values such as (email or cni).',
  })
  @Post()
  async create(@Body() newUserDto: NewUserDto) {
    return this.usersService.create(newUserDto);
  }

  @ApiOkResponse({
    description: 'User successfully updated.',
    type: User,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error occured.',
  })
  @ApiBadRequestResponse({
    description:
      'Bad Request. most often duplicated values such as (email or cni). Or user code invalid',
  })
  @ApiNotFoundResponse({
    description: 'User not found.',
  })
  @Put(':code')
  async update(
    @Param('code') code: string,
    @Body() updateUserDto: UpdateUserDto,
  ) {
    if (!isValidUserCode(code)) {
      throw new InvalidCodeException('User code is incorrect!');
    }
    return this.usersService.update(code, updateUserDto);
  }

  @ApiOkResponse({
    description: 'User successfully updated.',
    type: User,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error occured.',
  })
  @ApiBadRequestResponse({
    description: 'Bad Request. Invalid user code.',
  })
  @ApiNotFoundResponse({
    description: 'User not found.',
  })
  @Get(':code')
  async findOne(@Param('code') code: string) {
    if (!isValidUserCode(code)) {
      throw new InvalidCodeException('User code is incorrect!');
    }
    return this.usersService.findOne(code);
  }

  @ApiOkResponse({
    description: 'User successfully deleted.',
    type: User,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error occured.',
  })
  @ApiBadRequestResponse({
    description: 'Bad Request. Invalid user code.',
  })
  @ApiNotFoundResponse({
    description: 'User not found.',
  })
  @Delete(':code')
  async delete(@Param('code') code: string) {
    if (!isValidUserCode(code)) {
      throw new InvalidCodeException('User code is incorrect!');
    }
    return this.usersService.delete(code);
  }
}
