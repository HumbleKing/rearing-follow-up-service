import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import * as bcrypt from 'bcrypt';
import { Model } from 'mongoose';
import { fail, Result, succeed } from 'src/helpers/http-response';
// eslint-disable-next-line prettier/prettier
import { codeGenerator, ErrorMessages, generateDefaultPassword } from 'src/helpers/main.helper';
import { getDefaultUserInfos } from 'src/helpers/users.helper';

import { NewUserDto, UpdateUserDto } from './users.dto';
import { User, UserDocument } from './users.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.name)
    private readonly usersModel: Model<UserDocument>,
  ) {}

  async findAll(): Promise<Result> {
    try {
      return succeed({
        data:
          (await this.usersModel
            .find({ isDeleted: false }, '-_id -__v -password')
            .lean()) || [],
      });
    } catch (error) {
      throw new HttpException(
        ErrorMessages.ERROR_GETTING_DATA,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  async create(newUser: NewUserDto): Promise<Result> {
    try {
      const creationDate = new Date();
      const password = generateDefaultPassword();
      const salt = await bcrypt.genSalt();
      const user = {
        code: codeGenerator('USR'),
        firstName: newUser.firstName,
        lastName: newUser.lastName,
        email: newUser.email,
        cni: newUser.cni,
        password: await bcrypt.hash(password, salt),
        createdAt: creationDate,
        lastUpdatedAt: creationDate,
      };
      const createdUser = await this.usersModel.create(user);
      return succeed({
        code: HttpStatus.CREATED,
        data: {
          ...getDefaultUserInfos(createdUser),
          password: password,
        },
        message: 'New user successfully created.',
      });
    } catch (error) {
      if (error?.code === 11000) {
        throw new HttpException(
          `Please check if there is not already a user with the same email: ${newUser.email} or cni: ${newUser.cni}.`,
          HttpStatus.BAD_REQUEST,
        );
      } else {
        throw new HttpException(
          `Error while creating new user. Try again.`,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }

  async update(code: string, updateUser: UpdateUserDto): Promise<Result> {
    try {
      const filter = { code: code };
      const value = {
        ...updateUser,
        lastUpdatedAt: new Date(),
      };
      const updatedUser = await this.usersModel.findOneAndUpdate(
        filter,
        value,
        { new: true },
      );
      if (!updatedUser)
        return fail({
          error: `Not Found`,
          code: HttpStatus.NOT_FOUND,
          message: `user with code: ${code} not found`,
        });
      else return succeed({ data: getDefaultUserInfos(updatedUser) });
    } catch (error) {
      if (error?.code === 11000) {
        throw new HttpException(
          `Please check if there is not already a user with the same email: ${updateUser.email} or cni: ${updateUser.cni}.`,
          HttpStatus.BAD_REQUEST,
        );
      } else {
        throw new HttpException(
          `Error while creating new user. Try again.`,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }

  async findOne(code: string): Promise<Result> {
    try {
      const user = await this.usersModel
        .findOne({ code }, '-_id -__v -password')
        .lean();
      if (!user)
        return fail({
          error: `Not Found`,
          code: HttpStatus.NOT_FOUND,
          message: `user with code: ${code} not found`,
        });
      else return succeed({ data: user });
    } catch (error) {
      throw new HttpException(
        ErrorMessages.ERROR_GETTING_DATA,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  async delete(code: string): Promise<Result> {
    try {
      const filter = { code: code };
      const value = {
        isDeleted: true,
        lastUpdatedAt: new Date(),
      };
      const deletedUser = await this.usersModel
        .findOneAndUpdate(filter, value, { new: true })
        .lean();
      if (!deletedUser)
        return fail({
          error: `Not Found`,
          code: HttpStatus.NOT_FOUND,
          message: `user with code: ${code} not found`,
        });
      else
        return succeed({
          data: getDefaultUserInfos(deletedUser),
          message: 'User successfully deleted',
        });
    } catch (error) {
      throw new HttpException(
        ErrorMessages.ERROR_GETTING_DATA,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  async findUserByCode(code: string): Promise<UserDocument> {
    try {
      return await this.usersModel.findOne({ code }).lean();
    } catch (error) {
      throw new HttpException(
        ErrorMessages.ERROR_GETTING_DATA,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
